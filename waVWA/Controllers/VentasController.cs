﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using waVWA;

namespace waVWA.Controllers
{
    public class VentasController : Controller
    {
        private VWAEntities db = new VWAEntities();

        // GET: Ventas
        public ActionResult Index()
        {
            var venta = db.Venta.Include(v => v.Carroceria).Include(v => v.Cliente).Include(v => v.Color).Include(v => v.Linea).Include(v => v.Marca).Include(v => v.Modelo).Include(v => v.Segmento);
            return View(venta.ToList());
        }

        // GET: Ventas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venta venta = db.Venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        // GET: Ventas/Create
        public ActionResult Create()
        {
            ViewBag.id_carroceria = new SelectList(db.Carroceria, "id", "carroceria1");
            ViewBag.id_cliente = new SelectList(db.Cliente, "id", "infoCliente");
            ViewBag.id_color = new SelectList(db.Color, "id", "color1");
            ViewBag.id_linea = new SelectList(db.Linea, "id", "linea1");
            ViewBag.id_marca = new SelectList(db.Marca, "id", "marca1");
            ViewBag.id_modelo = new SelectList(db.Modelo, "id", "modelo1");
            ViewBag.id_segmento = new SelectList(db.Segmento, "id", "segmento1");
            return View();
        }

        // POST: Ventas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_cliente,id_segmento,id_marca,id_linea,id_carroceria,id_modelo,id_color,fecha,precio")] Venta venta)
        {
            if (ModelState.IsValid)
            {
                venta.fecha = DateTime.Now;
                db.Venta.Add(venta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_carroceria = new SelectList(db.Carroceria, "id", "carroceria1", venta.id_carroceria);
            ViewBag.id_cliente = new SelectList(db.Cliente, "id", "nombres", venta.id_cliente);
            ViewBag.id_color = new SelectList(db.Color, "id", "color1", venta.id_color);
            ViewBag.id_linea = new SelectList(db.Linea, "id", "linea1", venta.id_linea);
            ViewBag.id_marca = new SelectList(db.Marca, "id", "marca1", venta.id_marca);
            ViewBag.id_modelo = new SelectList(db.Modelo, "id", "modelo1", venta.id_modelo);
            ViewBag.id_segmento = new SelectList(db.Segmento, "id", "segmento1", venta.id_segmento);
            return View(venta);
        }

        // GET: Ventas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venta venta = db.Venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_carroceria = new SelectList(db.Carroceria, "id", "carroceria1", venta.id_carroceria);
            ViewBag.id_cliente = new SelectList(db.Cliente, "id", "infoCliente", venta.id_cliente);
            ViewBag.id_color = new SelectList(db.Color, "id", "color1", venta.id_color);
            ViewBag.id_linea = new SelectList(db.Linea, "id", "linea1", venta.id_linea);
            ViewBag.id_marca = new SelectList(db.Marca, "id", "marca1", venta.id_marca);
            ViewBag.id_modelo = new SelectList(db.Modelo, "id", "modelo1", venta.id_modelo);
            ViewBag.id_segmento = new SelectList(db.Segmento, "id", "segmento1", venta.id_segmento);
            return View(venta);
        }

        // POST: Ventas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_cliente,id_segmento,id_marca,id_linea,id_carroceria,id_modelo,id_color,fecha,precio")] Venta venta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(venta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_carroceria = new SelectList(db.Carroceria, "id", "carroceria1", venta.id_carroceria);
            ViewBag.id_cliente = new SelectList(db.Cliente, "id", "nombres", venta.id_cliente);
            ViewBag.id_color = new SelectList(db.Color, "id", "color1", venta.id_color);
            ViewBag.id_linea = new SelectList(db.Linea, "id", "linea1", venta.id_linea);
            ViewBag.id_marca = new SelectList(db.Marca, "id", "marca1", venta.id_marca);
            ViewBag.id_modelo = new SelectList(db.Modelo, "id", "modelo1", venta.id_modelo);
            ViewBag.id_segmento = new SelectList(db.Segmento, "id", "segmento1", venta.id_segmento);
            return View(venta);
        }

        // GET: Ventas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venta venta = db.Venta.Find(id);
            if (venta == null)
            {
                return HttpNotFound();
            }
            return View(venta);
        }

        // POST: Ventas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Venta venta = db.Venta.Find(id);
            db.Venta.Remove(venta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
