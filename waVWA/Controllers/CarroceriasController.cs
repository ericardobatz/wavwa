﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using waVWA;

namespace waVWA.Controllers
{
    public class CarroceriasController : Controller
    {
        private VWAEntities db = new VWAEntities();

        // GET: Carrocerias
        public ActionResult Index()
        {
            return View(db.Carroceria.ToList());
        }

        // GET: Carrocerias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Carroceria carroceria = db.Carroceria.Find(id);
            if (carroceria == null)
            {
                return HttpNotFound();
            }
            return View(carroceria);
        }

        // GET: Carrocerias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Carrocerias/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,carroceria1")] Carroceria carroceria)
        {
            if (ModelState.IsValid)
            {
                db.Carroceria.Add(carroceria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(carroceria);
        }

        // GET: Carrocerias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Carroceria carroceria = db.Carroceria.Find(id);
            if (carroceria == null)
            {
                return HttpNotFound();
            }
            return View(carroceria);
        }

        // POST: Carrocerias/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,carroceria1")] Carroceria carroceria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(carroceria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(carroceria);
        }

        // GET: Carrocerias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Carroceria carroceria = db.Carroceria.Find(id);
            if (carroceria == null)
            {
                return HttpNotFound();
            }
            return View(carroceria);
        }

        // POST: Carrocerias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Carroceria carroceria = db.Carroceria.Find(id);
            db.Carroceria.Remove(carroceria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
