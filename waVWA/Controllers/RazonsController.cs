﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using waVWA;

namespace waVWA.Controllers
{
    public class RazonsController : Controller
    {
        private VWAEntities db = new VWAEntities();

        // GET: Razons
        public ActionResult Index()
        {
            return View(db.Razon.ToList());
        }

        // GET: Razons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Razon razon = db.Razon.Find(id);
            if (razon == null)
            {
                return HttpNotFound();
            }
            return View(razon);
        }

        // GET: Razons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Razons/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,razon1")] Razon razon)
        {
            if (ModelState.IsValid)
            {
                db.Razon.Add(razon);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(razon);
        }

        // GET: Razons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Razon razon = db.Razon.Find(id);
            if (razon == null)
            {
                return HttpNotFound();
            }
            return View(razon);
        }

        // POST: Razons/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,razon1")] Razon razon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(razon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(razon);
        }

        // GET: Razons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Razon razon = db.Razon.Find(id);
            if (razon == null)
            {
                return HttpNotFound();
            }
            return View(razon);
        }

        // POST: Razons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Razon razon = db.Razon.Find(id);
            db.Razon.Remove(razon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
